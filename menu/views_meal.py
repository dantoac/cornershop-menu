from django.shortcuts import render
from .models import Menu, Meal, Employee_Menu, Employee
from .forms import MealForm
from django.shortcuts import redirect, reverse
from django.urls import reverse_lazy

from django.views.generic import CreateView, ListView, DeleteView, UpdateView




class ListMeal(ListView):
    model = Meal
    template_name = 'meal/list.html'

class CreateMeal(CreateView):
    model = Meal
    form_class = MealForm
    template_name = 'meal/new.html'
    success_url = reverse_lazy('meal_list')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'title': 'Agregando'
        })

        return context


class UpdateMeal(UpdateView):
    model = Meal
    form_class = MealForm
    template_name = 'meal/new.html'
    success_url = reverse_lazy('meal_list')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'title': 'Modificando'
        })

        return context


class DeleteMeal(DeleteView):
    model = Meal
    template_name = 'generic_confirm.html'
    success_url = reverse_lazy('meal_list')

