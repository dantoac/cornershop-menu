from django.urls import path

from . import views_meal, views_menu, views_employee_menu


urlpatterns = [
    # Meals
    path('meal/index/', views_meal.ListMeal.as_view(), name='meal_list'),
    path('meal/new/', views_meal.CreateMeal.as_view(), name='meal_create'),
    path('meal/update/<int:pk>/', views_meal.UpdateMeal.as_view(), name='meal_update'),
    path('meal/delete/<int:pk>/', views_meal.DeleteMeal.as_view(), name='meal_delete'),

    #Menu
    path('menu/index/', views_menu.ListMenu.as_view(), name='menu_list'),
    path('menu/new/', views_menu.CreateMenu.as_view(), name='menu_create'),
    path('menu/update/<int:pk>/', views_menu.UpdateMenu.as_view(), name='menu_update'),
    path('menu/delete/<int:pk>/', views_menu.DeleteMenu.as_view(), name='menu_delete'),

    path('menu/<str:uid>/', views_menu.TodayMenu.as_view(), name='menu_today'),

    # meal requests
    path('meal/requested/', views_employee_menu.ListEmployeeMenu.as_view(), name='requested_list'),
    path('meal/requested/<str:duedate>/', views_employee_menu.ListEmployeeMenu.as_view(), name='requested_list')

]
