from .models import Menu, Meal, Employee_Menu, Employee
from .forms import MealForm
from django.shortcuts import redirect, reverse
from django.urls import reverse_lazy

from django.views.generic import CreateView, ListView, DeleteView, UpdateView
from datetime import datetime
from django.utils import timezone


class ListEmployeeMenu(ListView):
    model = Employee_Menu
    template_name = 'employee_menu/list.html'
    #queryset = Employee_Menu.objects.all()

    def get_queryset(self):
        arg = self.request.GET.get('duedate')
        print (f'GEEET {self.request.GET}')
        qs = super().get_queryset()
        if arg:
            duedate = datetime.strptime(arg, '%Y-%m-%d')
            return qs.filter(menu__due_date=duedate)


    def get(self, request, duedate):
        return render(request, self.template_name, )
