from django.contrib import admin
from .models import Meal, Menu, Employee, Employee_Menu

# Register your models here.
#
admin.site.register(Meal)
admin.site.register(Menu)
admin.site.register(Employee)
admin.site.register(Employee_Menu)
