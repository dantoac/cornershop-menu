from django.shortcuts import render, HttpResponse
from django.utils import timezone
from .forms import EmployeeMenuForm
from django.contrib.auth.mixins import LoginRequiredMixin

from .models import Menu, Meal, Employee_Menu, Employee, User
from .forms import MenuForm
from django.shortcuts import redirect, reverse, get_object_or_404
from django.urls import reverse_lazy
from uuid import uuid4

from django.views.generic import CreateView, ListView, DeleteView, UpdateView, FormView
from datetime import datetime



class ListMenu(ListView):
    model = Menu
    template_name = 'menu/list.html'


class CreateMenu(CreateView):
    model = Menu
    form_class = MenuForm
    template_name = 'menu/new.html'
    success_url = reverse_lazy('menu_list')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'title': 'Agregando'
        })

        return context

class UpdateMenu(UpdateView):
    model = Menu
    form_class = MenuForm
    template_name = 'menu/new.html'
    success_url = reverse_lazy('menu_list')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'title': 'Modificando'
        })

        return context


class DeleteMenu(DeleteView):
    model = Menu
    template_name = 'generic_confirm.html'
    success_url = reverse_lazy('menu_list')



class TodayMenu(CreateView):
    model = Employee_Menu
    template_name = 'menu/view.html'
    form_class = EmployeeMenuForm
    success_url = '/gracias/'
    queryset = Menu.objects.all()


    # def get_queryset(self):
    #     return self.queryset.filter(uuid=self.request.get.GET('uid'))


    def get(self, request, uid):

        form = EmployeeMenuForm(request.POST or None)

        menu = get_object_or_404(self.queryset.filter(uuid=uid))

        self.extra_context = {
            'menu': menu,
            'form': form
        }

        return render(request, self.template_name, self.extra_context)


    def post(self, request, uid):
        employee = Employee.objects.get(user=self.request.user)
        menu = get_object_or_404(self.queryset.filter(uuid=uid, due_date=timezone.now()))

        form = EmployeeMenuForm(request.POST)
        form.instance.employee = employee

        form.instance.menu = menu

        '''Acá evaluamos en duro si el post se esta realizando antes d e las 11:00am
        utilizado localtime por defecto formateado en 24hr.
        Idealmente, el valor de corte ('11:00') debiera ir en alguna variable seteable
        desde el modelo o como parámetro de configuración de la app.
        '''

        is_before_lunch = timezone.localtime().time() <= datetime.strptime('11:00', '%H:%M').time()

        if is_before_lunch:

            if form.is_valid():
                form.save()


            self.extra_context = {
                'menu': menu,
                'form': form
            }

            return render(request, self.template_name, self.extra_context)
        else:
            return HttpResponse('El plazo para solicitar el menú ha expirado.')

    def form_valid(self, form):
        employee = Employee.objects.get(user=self.request.user)
        menu = get_object_or_404(self.queryset.filter(uuid=uid))

        form.instance.employee = employee
        form.instance.menu = menu
        return super().form_valid(form)
