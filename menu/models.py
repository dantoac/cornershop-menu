from django.db import models
from uuid import uuid4
from django.contrib.auth import get_user_model
from django.utils import timezone
from django.contrib.auth.models import AbstractUser
from django.urls import reverse

from django.core.exceptions import ValidationError

# Create your models here.

User = get_user_model()

class Meal(models.Model):
    """Nombres y descripciones de los platos que se servirán en el menú"""
    name = models.CharField(max_length=48, unique=True, help_text='Nombre del plato')
    description = models.TextField(max_length=500, blank=True, help_text='Detalle de los ingredientes')

    def __str__(self):
        return self.name


class Employee(models.Model):
    """Usuario del sistema es potencialmente un 'empleado' que necesita
    ser alimentado"""
    user = models.OneToOneField(User, help_text='Usuario', on_delete=models.CASCADE)
    phone_number = models.CharField(max_length=50, help_text='Número de teléfono')

class Menu(models.Model):
    """Corresponde al menú para un día específico"""

    due_date = models.DateField(default=timezone.now, unique=True)
    meals = models.ManyToManyField(Meal)
    employees = models.ManyToManyField(Employee, through='Employee_Menu')

    uuid = models.UUIDField(
        editable=False,
        default=uuid4,
        unique=True
        #primary_key=True
    )

    def __str__(self):

        due_date = self.due_date.strftime('%x')

        return f"Menú del {due_date}"

    class Meta:
        ordering = ['-due_date']

    def get_absolute_url(self):
        return reverse('menu_today', args=[self.uuid])


class Employee_Menu(models.Model):
    """Esta tabla guarda la selección de menú de un empleado.

    Se hace referencia al menú y al empleado, permitiendo a éste
    especificar alguna personalización.

    Importante: En el campo `meal_choice` se guarda directamente la opción
    elegida del menú. Aquí deliberadamente se desnormaliza `meal`
    para mantener un registro histórico de la opción seleccionada
    por el empleado, así no se actualiza si la referencia a `meal`
    cambiara en un futuro.
    """

    employee = models.ForeignKey(Employee, on_delete=models.CASCADE)
    menu = models.ForeignKey(Menu, on_delete=models.CASCADE)
    meal_choice = models.CharField(max_length=255, null=False)
    customization = models.TextField(max_length=500, blank=True,
                                     help_text='Desea especificar alguna instrucción especial para tu comida?')

    def validate_unique(self, *args, **kwargs):
        """valida que exista solamente un menú registrado para ese empleado ese día"""
        super(Employee_Menu, self).validate_unique(*args, **kwargs)

        if self.__class__.objects.\
                filter(employee=self.employee, menu__due_date=self.menu.due_date).\
                exists():
            raise ValidationError(
                message='Este empleado ya tiene un menú registrado para esta fecha',
                code='unique_together',
            )
