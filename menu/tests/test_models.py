from django.test import TestCase
from ..models import Meal, Menu
from datetime import datetime

class TestModels(TestCase):

    def setUp(self):
        """
        > Option 1: Corn pie, Salad and Dessert
        > Option 2: Chicken Nugget Rice, Salad and Dessert
        > Option 3: Rice with hamburger, Salad and Dessert
        > Option 4: Premium chicken Salad and Dessert.
        :return:
        """

        self.meals = [
            'Corn pie, Salad and Dessert',
            'Chicken Nugget Rice, Salad and Dessert',
            'Rice with hamburger, Salad and Dessert',
            'Premium chicken Salad and Dessert',
        ]

        self.today = datetime.utcnow().date()


    def test_create_meal(self):
        """Prueba registro de un Plato de comida. Cada plato de comida
        representa una opción en el menú"""
        meal = Meal()
        meal.name = self.meals[0]
        meal.save()
        first_saved = Meal.objects.get(pk=1)
        self.assertEqual(str(first_saved), str(meal))


    def test_create_menu_one_meal(self):
        """Prueba la asociación de un plato u opción al menú"""

        meal = Meal(name=self.meals[0])
        meal.save()
        menu = Menu(due_date=self.today)
        menu.save()
        menu.meals.add(meal)

        self.assertEqual(menu.due_date, self.today)
        self.assertEqual(len(menu.uuid.hex), 32)
        self.assertEqual(menu.meals.count(), 1)


    def test_create_one_menu_many_meals(self):
        """Prueba registro de varios platos a 1 menú"""

        menu = Menu(due_date='2020-02-04')
        menu.save()

        for d in self.meals:
            meal = Meal(name=d)
            meal.save()
            menu.meals.add(meal)
            menu.save()

        first_menu = Menu.objects.all().first()
        self.assertEqual(first_menu.meals.count(), len(self.meals))
        self.assertEqual(Menu.objects.count(), 1)
