from django.test import TestCase, Client
from django.urls import reverse
from datetime import datetime
from ..models import Meal, Menu

class TestUrl(TestCase):


    def setUp(self):
        """
        > Option 1: Corn pie, Salad and Dessert
        > Option 2: Chicken Nugget Rice, Salad and Dessert
        > Option 3: Rice with hamburger, Salad and Dessert
        > Option 4: Premium chicken Salad and Dessert.
        :return:
        """

        self.meals = [
            'Corn pie, Salad and Dessert',
            'Chicken Nugget Rice, Salad and Dessert',
            'Rice with hamburger, Salad and Dessert',
            'Premium chicken Salad and Dessert',
        ]

        self.today = datetime.utcnow().date()


    def test_get_menu_with_uuid(self):
        response = self.client.get('/menu/menu//24fd79dbf27d442485d0c0f201e1ebfe')
        self.assertEqual(response.status_code, 404)

        response = self.client.get('/menu/menu/aoeu')
        self.assertEqual(response.status_code, 301)

        response = self.client.get('/menu/menu/1234')
        self.assertEqual(response.status_code, 301)

        response = self.client.get('/menu/menu/2793b1ab-62cc-461d-a197-0a8cb0c43318')
        self.assertEqual(response.status_code, 301)



    ##### MENU


    def test_list_menu(self):
        response = self.client.get('/menu/menu/index/')
        self.assertEqual(response.status_code, 200)


    def create_menu_one_meal(self):

        meal = Meal(name=self.meals[0])
        meal.save()
        menu = Menu(due_date=self.today)
        menu.save()
        menu.meals.add(meal)


    def test_create_menu(self):
        response = self.client.get('/menu/menu/new/')
        self.assertEqual(response.status_code, 200)

    def test_edit_menu(self):
        #self.create_menu_one_meal()

        meal = Meal(name=self.meals[0])
        meal.save()
        menu = Menu(due_date=self.today)
        menu.save()
        menu.meals.add(meal)
        menu.save()
        response = self.client.get('/menu/update/1/')
        print ('aoeu', response)
        self.assertEqual(response.status_code, 200)


    ##### MEALS

    def test_list_meal(self):
        response = self.client.get('/menu/meal/index/')
        self.assertEqual(response.status_code, 200)

    def test_create_meal(self):
        response = self.client.get('/menu/meal/new/')
        self.assertEqual(response.status_code, 200)

    #### Uses
    def test_edit_menu(self):
        meal = Meal(name=self.meals[0])
        meal.save()
        menu = Menu(due_date=self.today)
        menu.save()
        menu.meals.add(meal)
        response = self.client.get('/menu/menu/update/1/')
        self.assertEqual(response.status_code, 200)

    def test_delete_menu(self):
        meal = Meal(name=self.meals[0])
        meal.save()
        menu = Menu(due_date=self.today)
        menu.save()
        menu.meals.add(meal)
        response = self.client.get('/menu/menu/delete/1/')
        self.assertEqual(response.status_code, 200)



    def test_access_others_employee_menu(self):
        pass

    def test_permission_create_menu_as_employee(self):
        pass

    def test_choose_menu_today(self):
        pass

    def test_choose_menu_not_today(self):
        pass

    def test_choose_menu_before_lunch(self):
        pass

    def test_choose_menu_after_lunch(self):
        pass


