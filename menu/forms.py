from django import forms
from .models import Meal, Menu, Employee, Employee_Menu

class MealForm(forms.ModelForm):
    class Meta:
        model = Meal
        fields = '__all__'


class MenuForm(forms.ModelForm):
    class Meta:
        model = Menu
        fields = ['due_date', 'meals']


class EmployeeMenuForm(forms.ModelForm):
    class Meta:
        model = Employee_Menu
        fields = ['menu', 'meal_choice', 'customization']
