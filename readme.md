Esta es una app en django para llevar registro de los almuerzos pedidos por los empleados de una empresa.

Funcionando:
 - CRUD opciones de menú
 - CRUD para un día determinado y asociarle opciones
 - Elegir almuerzo por parte de un Empleado
   - para el mismo día antes de las 11:00am
   - solamente 1 almuerzo por usuario para un día determinado
   - especificar alguna personalización
 - Acceder al menú mediante URL usando su UUID


ToDo:

 - Login y Permisos de usuario
 - CRUD usuarios y empleados
 - Tareas celery (recolección de menú diario)
 - Integración con slack y publicación de menú
 - Completar UNITTEST
